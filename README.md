
# Plantilla para explicaciones de metodo webflux
Esta plantilla describe el como se debe llenar cada README por nivel desde Junior hasta Senior, la idea esque en estos archivos README se describa y explique de forma detallada y ejemplificada, en lo posible con imagenes el uso que tiene una funcion de webflux por ejemplo .map, .flatmap etc....


# Seccionado
Cada README estara dentro de una subcarpeta dependiendo el nivel y tipo de metodos por ejemplo deberia existir un README dentro de transformacion > junior, adentro de esta ruta el README debe explicar todos los metodos o funciones del nivel junior al que se enfrentara en las actividades o miniretos de transformacion que tendran que solucionar.

# Estructura del del proyecto
Cada subseccion o seccion de las disponibles (junior-middle,advanced)  debe incluir un archivo readme(que es donde estara la explicacion de cada metodo tratado a nivel teorico y su uso)
, un archivo Java (que contendra los ejercicios que el pragmatico desarrollara como practica), y cada ejercicio de propuesto como reto debe tener su respectiva prueba unitaria para poder validar que el pragmatico logro cumplir el reto).
Cada seccion va por tipologia del metodo en el siguiente ejemplo la seccion es sobre los metodos de " transformacion" de webflux, los cuales pueden ser
map, flatmap y otros.


![img_1.png](images/img_1.png)

Lo ideal es que por metodo se desarrollen minimo 6 ejercicios del metodo, o del tipo de metodo si es uno de trasformacion como (flatmap, map), o otro estilo como FILTER,
repartidos de la siguiente manera ( 2 ejercicios de nivel Junior, 2 ejercicios nivel intermedio, 2 ejercicios nivel Advanced)
## Ejemplo de la estructura de Estructura del contenido
La idea es que cada readme que estara en cada subcarpeta tenga esta estructura
con el fin que las explicaciones sean claras y se siga un orden al momento de estudiar y repasar, la estructura a seguir en el 
readme es:
- [Titulo]
- [Descripcion]
- [Indice] - Tendra acceso directo a la explicacion de cada funcion que hay en el nivel
- [Explicacion por metodo]
- [conclusiones]