package com.pragma.training.webflux.webflux_methods_tests_v1.transformation.junior;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(MockitoExtension.class)
class JuniorWebfluxTransformationTest {
    @InjectMocks
    JuniorWebfluxTransformation juniorWebfluxTransformation;
    @ParameterizedTest
    @MethodSource("provideFluxesAndResults")
    void whenTransformarFluxACuadradoThenReturnCuadrado(Flux<Integer> flujoNums, Flux<Integer> expected) {
        Flux<Integer> result = juniorWebfluxTransformation.webFluxInteger(flujoNums);
        StepVerifier.create(result)
                .expectNextSequence(expected.toIterable())
                .verifyComplete();
    }
    @ParameterizedTest
    @MethodSource("provideStringParameters")
    void whenTryTransformStringFluxThenReturnFluxInteger(Flux<String> flujoNums, Flux<Integer> expected){
        Flux<Integer> result = juniorWebfluxTransformation.webFluxString(flujoNums);
        StepVerifier.create(result)
                .expectNextSequence(expected.toIterable())
                .verifyComplete();

    }
    @ParameterizedTest
    @MethodSource("providerMonoUpper")
    void whenTryTransformMonoUpperThenReturnMono(Mono<String> string, String expected){
        Mono<String> result = juniorWebfluxTransformation.monoToUpper(string);
        StepVerifier.create(result)
                .expectSubscription()
                .expectNext(expected)
                .verifyComplete();
    }
    @ParameterizedTest
    @MethodSource("provideFluxIntegerToNewFluxWithRange0UntilNum")
    void whenTransformFluxIntegerToNewFluxWithRange0UntilNum(Flux<Integer> source,Flux<Integer>expect){
        Flux<Integer> result = juniorWebfluxTransformation.webFluxFlatmapInteger(source);
        StepVerifier.create(result)
                .expectSubscription()
                .expectNextSequence(expect.toIterable())
                .verifyComplete();

    }
    private static Stream<Arguments> provideFluxIntegerToNewFluxWithRange0UntilNum() {
        return Stream.of(
                Arguments.of(Flux.just(1, 2, 3, 4, 5), Flux.just(0, 0, 1, 0, 1, 2, 0, 1, 2, 3, 0, 1, 2, 3, 4))
                //Arguments.of(Flux.just(6, 7, 8), Flux.just(36, 49, 64))
        );
    }

    private static Stream<Arguments> provideFluxesAndResults() {
        return Stream.of(
                Arguments.of(Flux.just(1, 2, 3, 4, 5), Flux.just(1, 4, 9, 16, 25)),
                Arguments.of(Flux.just(6, 7, 8), Flux.just(36, 49, 64))
        );
    }

    private static Stream<Arguments>provideStringParameters(){
        return Stream.of(Arguments.of(Flux.just("Fatima","Andrea","Gabriel","Juan"),Flux.just(6,6,7,4)),
                Arguments.of(Flux.just("lala","Lucia","Ana","Daniel","Lucas"),Flux.just(4,5,3,6,5))
        );
    }
    private static Stream<Arguments> providerMonoUpper(){
        return Stream.of(Arguments.of(Mono.just("fatima"),"FATIMA"),
                Arguments.of(Mono.just("lala"),"LALA"),
                Arguments.of(Mono.just("daniel"),"DANIEL")
        );
    }
}