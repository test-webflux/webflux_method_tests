package com.pragma.training.webflux.webflux_methods_tests_v1.transformation.junior.utils.impl;

import com.pragma.training.webflux.webflux_methods_tests_v1.transformation.junior.utils.SimulatedService;
import reactor.core.publisher.Flux;

import java.time.Duration;

public class SimulatedServiceImpl implements SimulatedService {
    @Override
    public Flux<String> getAllByItem(String init) {
        return Flux.just("Daniel","Test","Test2","Test3","Test4","Test5","Test6","Test7","Test8","Test9","Test10").filter(el -> el.startsWith(init))
                .delayElements(Duration.ofSeconds(1));

    }
}
