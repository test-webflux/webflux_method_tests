package com.pragma.training.webflux.webflux_methods_tests_v1.transformation.junior.utils;

import reactor.core.publisher.Flux;

public interface SimulatedService {
    Flux<String> getAllByItem(String init);
}
