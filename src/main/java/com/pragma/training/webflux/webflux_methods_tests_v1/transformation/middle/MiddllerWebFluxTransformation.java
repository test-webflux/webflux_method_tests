package com.pragma.training.webflux.webflux_methods_tests_v1.transformation.middle;

import reactor.core.publisher.Flux;

import java.util.List;

public class MiddllerWebFluxTransformation {
    //Crea un Flux de números enteros y usa el operador buffer para agrupar los números en listas de tamaño específico.
    public Flux<List<Integer>> createFluxList() {
        return Flux.range(1, 10).buffer(3);
    }
    //Crea un Flux de números enteros y usa el operador buffer pemitir datos en lotes cada 3 segundos
    //Crea un Flux de números enteros y usa el operador buffer para agrupar los números en listas de tamaño específico omitiendo los primeros 3 elementos de cada lote

    //Crea un Flux de cadenas de texto y usa el operador flatMap en combinación con Mono.delay para transformar cada cadena en un Mono que la emite después de un retardo.
    //Crea un Flux de números enteros y usa el operador window para agrupar los números en Flux de tamaño específico.
    //Crea un Flux de números enteros y usa el operador switchMap para transformar cada número en un Flux que emite números desde cero hasta ese número, pero que se interrumpe cuando el siguiente número llega.
    //Crea un Mono de una lista de números y usa el operador flatMapIterable para transformarlo en un Flux que emite los números de la lista.
}
