package com.pragma.training.webflux.webflux_methods_tests_v1;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import reactor.core.publisher.Flux;

import java.util.List;

@SpringBootApplication
public class WebfluxMethodsTestsV1Application implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(WebfluxMethodsTestsV1Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Flux<Integer> source = Flux.range(1, 10);
        Flux<List<Integer>> buffered = source.buffer(3);

        buffered.subscribe(System.out::println);


    }
}
