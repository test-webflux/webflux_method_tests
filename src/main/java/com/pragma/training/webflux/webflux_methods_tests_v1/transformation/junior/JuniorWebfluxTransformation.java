package com.pragma.training.webflux.webflux_methods_tests_v1.transformation.junior;

import com.pragma.training.webflux.webflux_methods_tests_v1.transformation.junior.utils.SimulatedService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class JuniorWebfluxTransformation {
    private final SimulatedService simulatedService;

    public JuniorWebfluxTransformation(SimulatedService simulatedService) {
        this.simulatedService = simulatedService;
    }

    //Crea un Flux de números enteros y usa el operador map para transformar cada número en su cuadrado.
    public Flux<Integer> webFluxInteger(Flux<Integer> flujoNums) {
        return flujoNums.map(num -> num * num);
    }


    //Crea un Flux de cadenas de texto y usa el operador map para transformar cada cadena en su longitud.
    public Flux<Integer> webFluxString(Flux<String> flujoStrings) {
        return flujoStrings.map(String::length);
    }

    //Crea un Mono que contenga una cadena de texto y usa el operador map para convertir la cadena a mayúsculas.
    public Mono<String> monoToUpper(Mono<String> mono) {
        return mono.map(String::toUpperCase);
    }

    //Crea un Flux de números enteros y usa el operador flatMap para transformar cada número en un rango de números desde cero hasta ese número.
    public Flux<Integer> webFluxFlatmapInteger(Flux<Integer> flujoNums) {
        return flujoNums.flatMap(num -> Flux.range(0, num));
    }

    //Crea un Mono que contenga una cadena de texto y usa el operador flatMap para transformar la cadena en un Flux de sus caracteres.
    public Flux<Character> monoToChars(Mono<String> mono) {
        return null;

    }

    //Utiliza flatMap para filtrar solo los números pares y luego multiplica cada número por 2.
    public Flux<Integer> webFluxFlatmapInteger2(Flux<Integer> flujoNums) {
        return flujoNums.filter(num -> num % 2 == 0).flatMap(num -> Flux.just(num * 2));
    }

    //Utiliza flatMap para realizar llamadas asíncronas a un servicio externo para obtener los datos que inicien segun cada dato de otro flujo
    public Flux<String> asyncFlatMapOperation(Flux<String> init) {
        return init.flatMap(this.simulatedService::getAllByItem);
    }



    //Utiliza flatMap para concatenar estos flujos en uno solo y lograr todas las combinaciones posibles
    public Flux<String> concatFlux(Flux<String> flujo1, Flux<String> flujo2) {
        return flujo1.flatMap(el -> flujo2.map(el2 -> el + el2));
    }

    //Crea un flujo de solicitudes HTTP.
    //Utiliza flatMap para realizar operaciones que pueden lanzar excepciones y maneja esos errores correctamente.

    //Divide un flujo de elementos en varios flujos basados en ciertas condiciones.
    public Flux<String> fluxFluxIntegerToString(Flux<Integer> flujoNums) {
        return flujoNums.flatMap(num -> Flux.just(num,num * 2).map(String::valueOf));
    }
}
